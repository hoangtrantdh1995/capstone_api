const BASE_URL = "https://633ec0720dbc3309f3bc56fd.mockapi.io";

// R (read)
function loadProductFromAPI() {
  return axios

    .get(`${BASE_URL}/webSales`)
    .then(function (res) {
      return res.data;
    })
    .catch(function (err) {
      console.log("err: ", err);
      return [];
    });
}

