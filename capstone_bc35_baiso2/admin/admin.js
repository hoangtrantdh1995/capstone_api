
function init() {
    loadProductFromAPI().then(data => {
        renderListProduct(data);
        console.log('data: ', data);

    });
};

function renderListProduct(data) {
    let contentHTML = "";
    data.forEach(function (item) {
        let productObj = new Product(
            item.id,
            item.name,
            item.price,
            item.screen,
            item.backCamera,
            item.frontCamera,
            item.img,
            item.desc,
            item.type
        )
        const product = JSON.stringify(productObj);
        console.log(product);
        contentHTML += `<tr>
            <td>${productObj.id}</td>
            <td>${productObj.name}</td>
            <td>${numberFormat(productObj.price)}</td>
            <td>${productObj.screen}</td>
            <td>${productObj.backCamera}</td>
            <td>${productObj.frontCamera}</td>
            <td><img width="50px" src="${productObj.img}" alt="" />
            </td>
            <td>${productObj.desc}</td>
            <td>${productObj.type}</td>
            <td>
                <button class="btn btn-danger" onclick="deleteProduct(${productObj.id})">Xóa</button>
                <button class="btn btn-info" onclick='openUpdateProduct(${product})'>Xem</button>
            </td>
        </tr>`
    })
    document.getElementById("tblDanhSachSP").innerHTML = contentHTML;
};

// lưu và cập nhật dùng chung hàm (sử dụng type="hidden" dùng ẩn "id")
function saveProduct() {
    var data = getFormData();
    const valid = validateData(data);
    if (!valid) return;
    if (data.id) {
        updateProductAPI(data.id, data)
            .then(function (res) {
                init();
            });
    } else {
        saveProductToAPI(data)
            .then(function (res) {
                init();
            });
    }
    closeModal();

};

function deleteProduct(id) {
    console.log(id);
    deleteProductAPI(id)
        .then(function (res) {
            init();
        });
};

function openUpdateProduct(data) {
    loadDataToForm(data);

};

function getFormData() {
    let id = document.getElementById("id").value.trim();
    let name = document.getElementById("name").value.trim();
    let price = document.getElementById("price").value.trim();
    let screen = document.getElementById("screen").value.trim();
    let backCamera = document.getElementById("backCamera").value.trim();
    let frontCamera = document.getElementById("frontCamera").value.trim();
    let img = document.getElementById("img").value.trim();
    let desc = document.getElementById("desc").value.trim();
    let type = document.getElementById("type").value.trim();
    console.log(type);

    // trả thông tin từ form về obj
    return new Product(
        id || undefined,
        name,
        price,
        screen,
        backCamera,
        frontCamera,
        img,
        desc,
        type
    )
};

function loadDataToForm(data) {
    $('#myModal').modal('show');
    document.getElementById("id").value = data.id;
    document.getElementById("name").value = data.name;
    document.getElementById("price").value = data.price;
    document.getElementById("screen").value = data.screen;
    document.getElementById("backCamera").value = data.backCamera;
    document.getElementById("frontCamera").value = data.frontCamera;
    document.getElementById("img").value = data.img;
    document.getElementById("desc").value = data.desc;
    document.getElementById("type").value = data.type;

};

// đóng form / rest form
function closeModal() {
    document.getElementById("id").value = "";
    document.getElementById("name").value = "";
    document.getElementById("price").value = "";
    document.getElementById("screen").value = "";
    document.getElementById("backCamera").value = "";
    document.getElementById("frontCamera").value = "";
    document.getElementById("img").value = "";
    document.getElementById("desc").value = "";
    document.getElementById("type").value = "";

    $('#myModal').modal('hide');
}

function resetSpan() {
    document.getElementById("valid-name").innerText = "";
    document.getElementById("valid-price").innerText = "";
    document.getElementById("valid-screen").innerText = "";
    document.getElementById("valid-backCamera").innerText = "";
    document.getElementById("valid-frontCamera").innerText = "";
    document.getElementById("valid-img").innerText = "";
    document.getElementById("valid-desc").innerText = "";
    document.getElementById("valid-type").innerText = "";
}
function closeForm() {
    resetSpan();
}

// đóng form
// $('#myModal').modal('hide');

// mở form
// $('#myModal').modal('show');

function numberFormat(number) {
    return `${new Intl.NumberFormat("en-HOSSDDG", {
        style: 'currency',
        currency: 'USD',
        minimumFractionDigits: 2,
    }).format(number)}`;
};

// validate
function validateData(list) {
    const validName = list.validateName();
    const validPrice = list.validatePrice();
    const validScreen = list.validateScreen();
    const validBackCamera = list.validateBackCamera();
    const validFrontCamera = list.validateFrontCamera();
    const validImg = list.validateImg();
    const validDesc = list.validateDesc();
    const validType = list.validateType();
    return validName && validPrice && validScreen && validBackCamera && validFrontCamera && validImg && validDesc && validType;
};


