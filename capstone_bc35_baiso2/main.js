function init() {
  onLoading();
  loadProductFromAPI().then((data) => {
    offloading();
    renderListProduct(data);
    reLoadCart();
  });
}
let onLoading = () => {
  document.getElementById("loading").style.display = "flex";
};
let offloading = () => {
  document.getElementById("loading").style.display = "none";
};
function renderListProduct(data) {
  let contentHTML = "";
  data.forEach(function (item) {
    let productObj = new Product(
      item.id,
      item.name,
      item.price,
      item.screen,
      item.backCamera,
      item.frontCamera,
      item.img,
      item.desc,
      item.type
    );

    contentHTML += `<div class="card">
    <div class="top-bar">
        <i class="fab fa-apple"></i>
        <em class="stocks">In Stock</em>
    </div>
    <div class="img-container">
        <img class="product-img" src="${productObj.img}" alt="">
        <div class="out-of-stock-cover"><span>Out Of Stock</span></div>
    </div>
    <div class="details">
        <div class="name-fav">
            <strong class="product-name">${productObj.name}</strong>
            <button onclick="this.classList.toggle(&quot;fav&quot;)" class="heart"><i class="fas fa-heart"></i></button>
        </div>
        <div class="wrapper">
            <h5>Camera trước: ${productObj.frontCamera}</h5>
            <h5>Camera sau: ${productObj.backCamera}</h5>
            <p>Mô tả: ${productObj.desc}</p>
        </div>
        <div class="purchase">
            <p class="product-price">${numberFormat(productObj.price)}</p>
            <span class="btn-add">
                <div>
                    <button onclick='addToCart(${JSON.stringify(productObj)})' class="add-btn">Add <i class="fas fa-chevron-right"></i></button>
                </div>
            </span>
        </div>
    </div>
</div>`;
  });
  document.getElementById("inforProducts").innerHTML = contentHTML;
}

function openSideNav(isShow = true) {
  if (isShow) {
    // show side nav
    showsSideNav();
  } else {
    // hide side nav
    hideSideNav();
    // document.getElementById("cart-items").innerHTML = `<span class="empty-cart">Looks Like You Haven't Added Any Product In The Cart</span>`
  }
}
function limitPurchase() {
  openSideNav();
  hideLimitQty()
}

function buy() {
  // if (renderCart()) {
  //   return
  // }
  rederPurchase()
  document.getElementById("invoice").style.display = "block";
  hideSideNav()
  document.getElementById("cover purchase-cover").style.display = "block";
}

function order() {
  rederOrder();
  document.getElementById("invoice").style.display = "none";
  document.getElementById("payment-done").style.display = "block";
}

function cancel() {
  document.getElementById("cover purchase-cover").style.display = "none";
  document.getElementById("invoice").style.display = "none";
  showsSideNav()
}

function okay() {
  document.getElementById("payment-done").style.display = "none";
  document.getElementById("cover purchase-cover").style.display = "none";
  clearCartLS()
  reLoadCart()
}

function numberFormat(number) {
  return `${new Intl.NumberFormat("en-HOSSDDG", {
    style: 'currency',
    currency: 'USD',
    minimumFractionDigits: 2,
  }).format(number)}`;
};

let chosseType = () => {
  onLoading();
  let valueElement = document.getElementById("typePhone").value;
  loadProductFromAPI()
    .then((res) => {
      offloading();
      var listProduct = res.filter((product) => {
        if (valueElement == 0) {
          return true
        }
        // toLowerCase() ~ chuyển đổi sang chữ thường (do khi ta xét === thì sẽ xét luôn từ kí tự)
        return product.type.toLowerCase() === valueElement;
      });
      renderListProduct(listProduct);
    });
};
